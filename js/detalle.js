/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



//-- guardo el id del pokemon al que le hice click
window.onload = function(){
	window.pokenumero=(localStorage.getItem("pokeId"));
};
	
//Creo el objeto XMLHttpRequest 
const xhr = new XMLHttpRequest();

//Cuando se descargue el archivo...
xhr.onload = function() {
    //Si hay error lo informo
    if (xhr.status != 200) {
        alert("Error al intentar consultar pokedex.json");
        return;
    }
    //Convierto el JSON en un array de objetos
    let dataArray = JSON.parse(xhr.responseText);
    mostrarResultados(dataArray);
};

xhr.onerror = function(event) {
    console.log("error");
};

//Especifico el método 
xhr.open('GET', 'data/pokedex.json', true);
//Envío la solicitud
xhr.send();



function mostrarResultados(dataArray) {
    let items = document.getElementById("items");
    items.innerHTML = "";

	var pokemon=dataArray[window.pokenumero-1]; //--los id empiezan desde el 1, el array desde 0
   						    //-- anda pero no me convece, puede haber otra forma  
        
        //Creo el div "name"
        let divName = document.createElement('div');
        divName.setAttribute("class", "name");
        divName.innerText = pokemon.name;

        //Creo un ul para todos los tipos
        let ulType = document.createElement("ul");
        ulType.setAttribute("class", "type");

        //Creo un list item por cada type
        pokemon.type.forEach(type => {
            let liType = document.createElement('li');
            liType.setAttribute("class", type);
            liType.innerText += type;
            ulType.append(liType);
        }); 

        //Creo un ul para las estadísticas
        let ulStats = document.createElement("ul");
        ulStats.setAttribute("class", "stats");
        
        //Agrego HP
        let liHP = document.createElement("li");
        liHP.innerText = "HP: " + pokemon.base.HP;
        ulStats.append(liHP);

        //Agrego Attack
        let liAttack = document.createElement("li");
        liAttack.innerText = "Ataque:" + pokemon.base.Attack;
        ulStats.append(liAttack);

        //Agrego Defense
        let liDefense = document.createElement("li");
        liDefense.innerText = "Defensa: " + pokemon.base.Defense;
        ulStats.append(liDefense);
        
        
        
        //--Agrego Ataque especial
        let liSpAttack = document.createElement("li");
        liSpAttack.innerText = "Ataque Sp:" + pokemon.base["Sp. Attack"];
        ulStats.append(liSpAttack);

        //--Agrego Defensa especial
        let liSpDefense = document.createElement("li");
        liSpDefense.innerText = "Defensa Sp: " + pokemon.base["Sp. Defense"];
        ulStats.append(liSpDefense);
        
        

        //Agrego Speed
        let liSpeed = document.createElement("li");
        liSpeed.innerText = "Velocidad: " + pokemon.base.Speed;
        ulStats.append(liSpeed);

        //Creo el elemento a que contendrá cada pokemon
        let a = document.createElement('a');
        a.setAttribute("href", "index.html");
        a.setAttribute("class", "pokemon col col-lg-3 col-md-6 col-sm-12 col-xs-12");

        //Creo el div contenedor
        let divContainer = document.createElement('div');
        divContainer.setAttribute("class", "contenedor");
        
        //Meto dentro del tag a un contenedor
        a.appendChild(divContainer);

    
        //--Creo un elemento imagen y le asigno el thumbnail
        let img = document.createElement('img');
        img.setAttribute('src', pokemon.picture);
        img.setAttribute("class","imagen");
        

    
        //--creo un elemento imagen para el sprite
        let sprite= document.createElement('img');
        sprite.setAttribute('src', pokemon.sprite);
        sprite.setAttribute("class","sprite");
       
       
        //Meto dentro del contenedor todos los elementos
        divContainer.append(img);    
        divContainer.append(sprite);
        divContainer.append(divName);   
        divContainer.appendChild(ulType);

        divContainer.appendChild(ulStats);

        items.append(a);
        
        /*
         entonces CREO que seria como hacer algo asi en el html
          
          <div class="contenedor">
            <img> laImagendelpokemon </img>
        
            <ul>
                <li>tipo1</li>
                <li>tipo2</li>
            </ul>
            
            <img>direccion del sprite</img>
            
            <ul>
                <li>defensa</li>
                <li>ataque</li>
                <li>etc</li>
            </ul>
       
            
         </div>
         */        

};



